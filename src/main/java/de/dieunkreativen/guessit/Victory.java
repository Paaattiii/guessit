package de.dieunkreativen.guessit;

import java.util.Collection;
import java.util.List;
import java.util.TreeMap;

import org.bukkit.ChatColor;

import mc.alk.arena.competition.match.Match;
import mc.alk.arena.objects.ArenaPlayer;
import mc.alk.arena.objects.scoreboard.ArenaDisplaySlot;
import mc.alk.arena.objects.scoreboard.ArenaObjective;
import mc.alk.arena.objects.scoreboard.ArenaScoreboard;
import mc.alk.arena.objects.teams.ArenaTeam;
import mc.alk.arena.objects.victoryconditions.VictoryCondition;
import mc.alk.arena.objects.victoryconditions.interfaces.DefinesLeaderRanking;

public class Victory extends VictoryCondition implements DefinesLeaderRanking {
	final ArenaObjective scores;

	public Victory(Match match) {
		super(match);
		scores = new ArenaObjective("FlagCaptures", "Capture the Flag");
		scores.setDisplayName(ChatColor.GOLD+"GuessIt");
		ArenaScoreboard scoreboard = match.getScoreboard();
		scores.setDisplaySlot(ArenaDisplaySlot.SIDEBAR);
		scoreboard.addObjective(scores);
		scores.setDisplayTeams(false);
	}
	
	public void resetScores(){
		scores.setAllPoints(match, 0);
		
	}

	public void addScore(ArenaPlayer ap, int points) {
		scores.addPoints(ap, points);
	}
	
	public TreeMap<?, Collection<ArenaTeam>> getRanks() {
		return scores.getRanks();
	}
	
	public List<ArenaTeam> getLeaders() {
		return scores.getLeaders();
	}
}
