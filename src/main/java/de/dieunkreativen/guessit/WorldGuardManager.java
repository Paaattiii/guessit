package de.dieunkreativen.guessit;

import mc.alk.arena.util.plugins.WorldGuardUtil;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;

import com.sk89q.worldedit.Vector;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class WorldGuardManager {
	 public static WorldGuardPlugin getWorldGuard() {
		    Plugin plugin = GuessIt.getSelf().getServer().getPluginManager().getPlugin("WorldGuard");
		     
		    if ((plugin == null) || (!(plugin instanceof WorldGuardPlugin))) {
		    	return null;
		    }
		     	return (WorldGuardPlugin)plugin;
		    }
		    
	 public static Location getLocation(String arena, World world) {
		 ProtectedRegion pr = getWorldGuard().getRegionManager(world).getRegion("ba-" + arena);
		 if (pr == null) {
			 return null;
		 }
		 int minX = pr.getMinimumPoint().getBlockX();
		 int minZ = pr.getMinimumPoint().getBlockZ();
		 int minY = pr.getMinimumPoint().getBlockY();
				
		 int maxX = pr.getMaximumPoint().getBlockX();
		 int maxZ = pr.getMaximumPoint().getBlockZ();
		 int maxY = pr.getMaximumPoint().getBlockY();
				
		 double centerX = ((maxX - minX) / 2) + minX;
		 double centerZ = ((maxZ - minZ) / 2) + minZ;
		 double centerY = ((maxY - minY) / 2) + minY;
		 
		 return new Location(world, centerX, centerY, centerZ);
	 }
	 
	 @SuppressWarnings("deprecation")
	public static void clearArena(String arena, World world) {
		 try {
			 ProtectedRegion pr = getWorldGuard().getRegionManager(world).getRegion("ba-" + arena);
			 WorldGuardUtil.pasteSchematic(Bukkit.getConsoleSender(), pr, pr.getId(), world);
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
	 }
	 
	 public static String getRegionFromLocation(Location loc) {
		      // Make sure that a WorldGuard instance was found
				 if (getWorldGuard() != null) {
					 // Get the players position (as a WorldEdit Vector)
					 Vector newVector = new Vector(loc.getX(), loc.getY(), loc.getZ());     
		       
					 // Get WorldGuards Region Manager
					 RegionManager rm = getWorldGuard().getRegionManager(loc.getWorld());
		       
					 // Make sure a RegionManager was found
					 if (rm != null)    {
						 ApplicableRegionSet set = rm.getApplicableRegions(newVector);
						 for (ProtectedRegion each : set)
							return each.getId();
		             }
				 }
				 return null;
			 }
}
