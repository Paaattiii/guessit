package de.dieunkreativen.guessit;

import mc.alk.arena.objects.ArenaPlayer;
import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import net.minecraft.server.v1_10_R1.IChatBaseComponent.ChatSerializer;

import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;

public class Actionbar {

	public static void sendText(ArenaPlayer ap, String message) {
		message = ChatColor.translateAlternateColorCodes('&', message);
        IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \""+ message +"\"}");
        PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc,(byte) 2);
        ((CraftPlayer) ap.getPlayer()).getHandle().playerConnection.sendPacket(ppoc);
	}
}
