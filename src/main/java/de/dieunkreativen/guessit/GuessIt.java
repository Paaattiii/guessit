package de.dieunkreativen.guessit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Logger;

import mc.alk.arena.BattleArena;
import mc.alk.arena.objects.victoryconditions.VictoryType;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.configuration.file.FileConfiguration;

public class GuessIt extends JavaPlugin {
	static GuessIt plugin;
	Logger log;
	
	
    @Override
    public void onLoad() {
    	log = getLogger();
    }
    
	@Override
	public void onEnable() {
		plugin = this;
		PluginDescriptionFile pdfFile = this.getDescription();
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled!");
		
		VictoryType.register(Victory.class, this);
		BattleArena.registerCompetition(this, "GuessIt", "gi", GuessItArena.class);

		saveResource("words.txt", false);
		
        saveDefaultConfig();
        loadConfig();
        loadWords();
	}
	@Override
	public void onDisable() {
		log.info("GuessIt is disabled!");
	
	}
	
	public static GuessIt getSelf() {
		return plugin;
	}
	
    @Override
    public void reloadConfig(){
    	super.reloadConfig();
    	if (loadConfig()) {
    		log.info("Your config.yml file is up to date");
    	}
    	else {
    		log.warning("Your config.yml file is not up to date. Please delete your current file and restart the server");
    	}
    }
	
    public boolean loadConfig(){
    	if (this.getConfig().getDouble("configVersion") == 1.0){
    		FileConfiguration config = getConfig();
            return true;
    	}
    	else {
    		return false;   
       }
        
    }
    
	public void loadWords() {
		BufferedReader br = null;
		try {
			
			br = new BufferedReader(new InputStreamReader(new FileInputStream(GuessIt.getSelf().getDataFolder() + File.separator + "words.txt"), "UTF-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				GuessItArena.words.add(line);
			}
		} catch (IOException e) {
		    e.printStackTrace();
		} finally {
		    try {
		        br.close();
		    } catch (IOException e) {
		        e.printStackTrace();
		    }
		}
	}
	
	
	
}
	

	

	



