package de.dieunkreativen.guessit;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

import mc.alk.arena.BattleArena;
import mc.alk.arena.objects.ArenaPlayer;
import mc.alk.arena.objects.arenas.Arena;
import mc.alk.arena.objects.events.ArenaEventHandler;
import mc.alk.arena.objects.events.EventPriority;
import mc.alk.arena.objects.victoryconditions.VictoryCondition;

public class GuessItArena extends Arena {
	Victory scores;
	int task1, task2;
	Location lastLocation;
	Player lastPlayer;
	ArrayList<Player> alreadyPlayed = new ArrayList<Player>();
	ArrayList<Player> guessPlayer = new ArrayList<Player>();
	static ArrayList<String> words = new ArrayList<String>();
	Map<Player, Integer> score = new HashMap<Player, Integer>();
	String guessWord;
	
    @Override
    public void onOpen() {
    	VictoryCondition vc = getMatch().getVictoryCondition(Victory.class);
    	scores = (Victory) (vc != null ? vc : new Victory(getMatch()));
    	getMatch().addVictoryCondition(scores);
    	scores.resetScores();
    }
	
	public void onStart() {
		if (words.size() > 20) {
			GuessIt.getSelf().loadWords();
		}
		for (ArenaPlayer ap: getMatch().getAlivePlayers()) {
			ap.getPlayer().setGameMode(GameMode.ADVENTURE);
		}
		selectPlayer();
	}
	
	public void onFinish() {
		Bukkit.getScheduler().cancelTask(task1);
		Bukkit.getScheduler().cancelTask(task2);
		score.clear();
		resetArena();
		alreadyPlayed.clear();
	}
	
	public void selectPlayer() {
		if (alreadyPlayed.size() >= getMatch().getAlivePlayers().size()) {
			//Alle Spieler waren dran
			getWinner();
			return;
		}
		
		for (ArenaPlayer ap: getMatch().getAlivePlayers()) {
			if (!alreadyPlayed.contains(ap.getPlayer())) {
				teleportPlayer(ap.getPlayer());
				timer();
				break;
			}
		}
	}
	
	public void resetArena() {
		WorldGuardManager.clearArena(getMatch().getArena().getName(), Bukkit.getWorld(getMatch().getArena().getWorldGuardRegion().getWorldName()));
		guessPlayer.clear();
		guessWord = null;
	}
	
	public void timer() {
		task1 = Bukkit.getScheduler().scheduleSyncRepeatingTask(GuessIt.getSelf(), new Runnable() {
			int timer = 120;
			public void run() {
                for (ArenaPlayer ap : getMatch().getAlivePlayers()) {
                    Actionbar.sendText(ap, (ChatColor.GREEN + String.valueOf(timer - 1) + " Sekunden verbleibend"));
                }
                timer--;
                
                if (!isPlayerOnline(lastPlayer.getName())) {
                	alreadyPlayed.remove(lastPlayer);
                	broadcastMessage(ChatColor.YELLOW + lastPlayer.getName() + ChatColor.RED + " hat das Spiel verlassen");
                	timer = 0;
                }
                else if (guessPlayer.size() == getMatch().getAlivePlayers().size() - 1) {
                	timer = 0;
                }
                else if (guessPlayer.size() >= 3 && timer > 20) {
                	timer = 20;
                }
                else if (guessPlayer.size() >= 1 && timer > 40) {
                	timer = 40;
                }
                
                if (timer == 0) {
                	if (isPlayerOnline(lastPlayer.getName())) {
                		teleportPlayerBack(lastPlayer);
                		if (guessPlayer.size() >= 1) {
                			addPoints(lastPlayer, 2);
                			broadcastMessage("&2Das Wort wurde erraten! &e2 Punkte f�r " + lastPlayer.getName());
                		}
                	}
                	broadcastMessage("&2Das Wort war&e " + guessWord);
                	resetArena();
                	Bukkit.getScheduler().cancelTask(task1);
                	task2 = Bukkit.getScheduler().scheduleSyncDelayedTask(GuessIt.getSelf(), new Runnable() {
                		public void run() {
                			selectPlayer();
                			Bukkit.getScheduler().cancelTask(task2);
                		}
                	}, 40L); 
                }
            }
		}, 0L, 20L);
	}
	
	public void teleportPlayer(Player p) {
		alreadyPlayed.add(p);
		lastLocation = p.getLocation();
		lastPlayer = p;
		p.sendMessage(ChatColor.YELLOW + "Dein Wort: " + ChatColor.RED + getRandomWord());
		TitleUtil.sendTitle(p, "&4" + guessWord, 20, 50, 20);
		p.setGameMode(GameMode.CREATIVE);
		p.teleport(WorldGuardManager.getLocation(getMatch().getArena().getName(), p.getWorld()));
	}
	
	public void teleportPlayerBack(Player p) {
		p.getPlayer().getInventory().clear();
		p.setGameMode(GameMode.ADVENTURE);
		p.teleport(lastLocation);
	}
	
	public String getRandomWord() {
		Random rand = new SecureRandom();
		int x = rand.nextInt(words.size());
		guessWord = words.get(x);
		words.remove(x);
	    return guessWord;
	}
	
	public void addPoints(Player p, int points) {
		scores.addScore(BattleArena.toArenaPlayer(p), points);
		if (score.containsKey(p)) {
			score.put(p, score.get(p) + points);
		}
		else {
			score.put(p, points);
		}
	}
	
	public boolean isPlayerOnline(String name){
		 for (ArenaPlayer ap : getMatch().getAlivePlayers()) {
			if (ap.getName().equalsIgnoreCase(name))
				return true;
		}
		return false;
	}
	
	public void rightAnswer(Player p) {
		guessPlayer.add(p);
		p.playSound(p.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER , 1F, 0);
		if (guessPlayer.size() == 1) {
			addPoints(p, 3);
			broadcastMessage("&e" + p.getName() + " &2hat es erraten und daf�r&e " + 3 + " Punkte erhalten!");
			return;
		}
		else if (guessPlayer.size() == 2) {
			addPoints(p, 2);
			broadcastMessage("&e" + p.getName() + " &2hat es erraten und daf�r&e " + 2 + " Punkte erhalten!");
			return;
		}
		addPoints(p, 1);
		broadcastMessage("&e" + p.getName() + " &2hat es erraten und daf�r&e " + 1 + " Punkt erhalten!");
	}
	
	public void broadcastMessage(String msg) {
		 for (ArenaPlayer ap : getMatch().getAlivePlayers()) {
             ap.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
         }
	}
	
	public boolean canBuild(Location loc) {
		if (WorldGuardManager.getRegionFromLocation(loc) != null) {
			if (WorldGuardManager.getRegionFromLocation(loc).equalsIgnoreCase("ba-" + getMatch().getArena().getName())) {
				return true;
			}
		}
		return false;
	}
	
	public int levenshteinDistance(String s0, String s1) {                          
	    int len0 = s0.length() + 1;                                                     
	    int len1 = s1.length() + 1;                                                     
	 
	    // the array of distances                                                       
	    int[] cost = new int[len0];                                                     
	    int[] newcost = new int[len0];                                                  
	 
	    // initial cost of skipping prefix in String s0                                 
	    for (int i = 0; i < len0; i++) cost[i] = i;                                     
	 
	    // dynamicaly computing the array of distances                                  
	 
	    // transformation cost for each letter in s1                                    
	    for (int j = 1; j < len1; j++) {                                                
	        // initial cost of skipping prefix in String s1                             
	        newcost[0] = j;                                                             
	 
	        // transformation cost for each letter in s0                                
	        for(int i = 1; i < len0; i++) {                                             
	            // matching current letters in both strings                             
	            int match = (s0.charAt(i - 1) == s1.charAt(j - 1)) ? 0 : 1;             
	 
	            // computing cost for each transformation                               
	            int cost_replace = cost[i - 1] + match;                                 
	            int cost_insert  = cost[i] + 1;                                         
	            int cost_delete  = newcost[i - 1] + 1;                                  
	 
	            // keep minimum cost                                                    
	            newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
	        }                                                                           
	 
	        // swap cost/newcost arrays                                                 
	        int[] swap = cost; cost = newcost; newcost = swap;                          
	    }                                                                               
	 
	    // the distance is the cost for transforming all letters in both strings        
	    return cost[len0 - 1];                                                          
	}
	
	public void getWinner() {
		Entry<Player,Integer> winner = null;

		for(Entry<Player,Integer> entry : score.entrySet()) {
		    if (winner == null || entry.getValue() > winner.getValue()) {
		        winner = entry;
		    }
		}
		getMatch().setVictor(BattleArena.toArenaPlayer(winner.getKey()));
	}
	
    @ArenaEventHandler(priority = EventPriority.HIGHEST)
	public void onBlockPlace(BlockPlaceEvent event) {
		if (!canBuild(event.getBlock().getLocation())) {
			event.setCancelled(true);
		}
	}
	
	@ArenaEventHandler(priority = EventPriority.HIGHEST)
	public void onBlockDestroy(BlockBreakEvent event) {
		if (!canBuild(event.getBlock().getLocation())) {
			event.setCancelled(true);
		}
		
	}
	
	@ArenaEventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		event.setCancelled(true);
	}
	
	@ArenaEventHandler
	public void onSignChange(SignChangeEvent event) {
		event.setCancelled(true);
	}
	
	@ArenaEventHandler
	public void onPlayerItemDrop(PlayerDropItemEvent event) {
		event.setCancelled(true);
	}
	
	@ArenaEventHandler
	public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event) {
		Location loc = event.getBlockClicked().getLocation();
		loc.setY(loc.getY() + 1);
		
		if (!canBuild(loc)) {
			event.setCancelled(true);
		}
	}
	
	@ArenaEventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		Player p = event.getPlayer();
		String msg = event.getMessage();
		if (lastPlayer == p) {
			p.sendMessage(ChatColor.RED + "Du darfst den Chat nicht benutzen!");
			event.setCancelled(true);
			return;
		}
		if (guessPlayer.contains(p)) {
			p.sendMessage(ChatColor.RED + "Du hast das Wort bereits erraten!");
			event.setCancelled(true);
			return;
		}
		if (levenshteinDistance(guessWord, msg) <= 2) {
			if (!guessPlayer.contains(p)) {
				rightAnswer(p);				
			}
		}
		else {
			broadcastMessage(p.getDisplayName() + " " + msg);
		}
		event.setCancelled(true);
	}
	
	
}
